package MQTT

import (
	"crypto/tls"
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"time"
)

var defaultQoS byte = 2

type DebugLevel int

type Connection struct {
	mqttClient              mqtt.Client
	options                 *mqtt.ClientOptions
	subscriberQoS           byte
	publisherQoS            byte
	performanceDebug        bool
	PerformanceDebugChannel chan string
	debug                   bool
	DebugChannel            chan string
}

type fn func(interface{}) string

func pahoMqttClientOptions(clientId string, hostname string, port int) *mqtt.ClientOptions {
	options := mqtt.NewClientOptions()
	options.AddBroker(fmt.Sprintf("tcp://%s:%d", hostname, port))
	options.SetClientID(clientId)

	return options
}

func NewMqttConnection(clientId string, hostname string, port int) *Connection {
	options := pahoMqttClientOptions(clientId, hostname, port)
	return &Connection{
		mqttClient:              nil,
		options:                 options,
		PerformanceDebugChannel: make(chan string),
		DebugChannel:            make(chan string),
		subscriberQoS:           defaultQoS,
		publisherQoS:            defaultQoS,
		performanceDebug:        false,
		debug:                   false,
	}
}

func (c *Connection) connect() error {
	if c.debug {
		c.DebugChannel <- fmt.Sprintf("%+v", *c.options)
	}
	c.mqttClient = mqtt.NewClient(c.options)
	token := c.mqttClient.Connect()
	for !token.WaitTimeout(5 * time.Second) {
	}
	if err := token.Error(); err != nil {
		return err
	}
	return nil
}

func (c *Connection) GetClientId() string {
	return c.options.ClientID
}

func (c *Connection) SetUsername(username string) *Connection {
	c.options.SetUsername(username)
	return c
}

func (c *Connection) SetPassword(password string) *Connection {
	c.options.SetPassword(password)
	return c
}

func (c *Connection) SetTLSConfig(tlsConfig *tls.Config) *Connection {
	c.options.SetTLSConfig(tlsConfig)
	return c
}

func (c *Connection) SetPersistentClient(persistent bool) *Connection {
	c.options.SetCleanSession(!persistent)
	return c
}

func (c *Connection) SetSubscriberQoS(qos byte) *Connection {
	c.subscriberQoS = qos
	return c
}

func (c *Connection) SetPublisherQos(qos byte) *Connection {
	c.publisherQoS = qos
	return c
}

func (c *Connection) SetDebugging(state bool) *Connection {
	c.debug = state
	return c
}

func (c *Connection) Subscribe(topic string, output chan []byte) error {
	if c.mqttClient == nil {
		if err := c.connect(); err != nil {
			return err
		}
	}

	c.mqttClient.Subscribe(topic, c.subscriberQoS, func(client mqtt.Client, msg mqtt.Message) {
		output <- msg.Payload()
	})
	return nil
}

func (c *Connection) Publish(topic string, retained bool, input chan string) error {
	if c.mqttClient == nil {
		if err := c.connect(); err != nil {
			return err
		}
	}
	go func() {
		var beforePublish time.Time
		for msg := range input {
			if c.performanceDebug {
				beforePublish = time.Now()
			}
			token := c.mqttClient.Publish(topic, c.publisherQoS, retained, msg)
			token.Wait()
			if c.performanceDebug {
				c.PerformanceDebugChannel <- fmt.Sprintf("publish_delay: %d", time.Now().Sub(beforePublish).Nanoseconds())
			}
			if c.debug {
				c.DebugChannel <- fmt.Sprintf("mqtt_publish: Message sent")
			}
		}
	}()

	return nil
}

func (c *Connection) PublishWithTransform(topic string, retained bool, input chan interface{}, transform fn) error {
	if c.mqttClient == nil {
		if err := c.connect(); err != nil {
			return err
		}
	}
	go func() {
		var beforePublish time.Time
		for msg := range input {
			if c.performanceDebug {
				beforePublish = time.Now()
			}
			token := c.mqttClient.Publish(topic, c.publisherQoS, retained, transform(msg))
			token.Wait()
			if c.performanceDebug {
				c.PerformanceDebugChannel <- fmt.Sprintf("publish_delay: %d", time.Now().Sub(beforePublish).Nanoseconds())
			}
			if c.debug {
				c.DebugChannel <- fmt.Sprintf("mqtt_publish: Message sent")
			}
		}
	}()

	return nil
}
