package main

import (
	"fmt"
	"gitlab.com/blissfulreboot/golang/conffee"
)

type ExampleSubObject struct {
	SomeSubValue string
}

type ExampleConfiguration struct {
	SomeString string
	SomeInteger int
	SomeInteger8 int8
	SomeSubObject ExampleSubObject
}

func main() {
	conffee.Debug = true
	variable := ExampleConfiguration{
		SomeString:    "",
		SomeInteger:   0,
		SomeInteger8:  0,
		SomeSubObject: ExampleSubObject{
			SomeSubValue: "",
		},
	}
	conffee.ReadConfiguration("./example-configuration.json", &variable, true, true)
	fmt.Println(fmt.Sprintf("%+v", variable))
}
