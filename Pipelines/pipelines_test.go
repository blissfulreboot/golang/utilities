package Pipelines

import "testing"

func a(param interface{}) (interface{}, error) {
	integer, _ := param.(int)
	return integer + 1, nil
}

func panicingFunction(param interface{}) (interface{}, error) {
	panic("PANIC!")
}

func TestPipeline(t *testing.T) {
	pipeline := New()
	pipeline.AddStage(a).
		AddStage(a).
		AddStage(a)
	retVal, err := pipeline.Run(1)
	value := retVal.(int)
	if err != nil {
		t.Error(err)
	}
	if value != 4 {
		t.Errorf("pipeline returned %v instead of 4", retVal)
	}
}

func TestFailingPipeline(t *testing.T) {
	pipeline := New()
	pipeline.AddStage(a).
		AddStage(panicingFunction).
		AddStage(a)
	retVal, err := pipeline.Run(1)
	if err == nil {
		t.Error(err)
	}
	if retVal != nil {
		t.Errorf("when panicking, retval should be nil instead of %v", retVal)
	}
}