package Pipelines

import (
	"errors"
	"runtime/debug"
)

type PipelineFunction func(interface{}) (interface{}, error)

type Pipeline interface {
	AddStage(function PipelineFunction) *pipeline
	Run(interface{}) error
}

type pipeline struct {
	Functions []PipelineFunction
}

func New() *pipeline{
	return &pipeline{
		Functions: nil,
	}
}

func(p *pipeline) AddStage(f PipelineFunction) *pipeline{
	p.Functions = append(p.Functions, f)
	return p
}

func(p *pipeline) Run(input interface{}) (retVal interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			retVal = nil
			err = errors.New("Pipeline panicked: " + string(debug.Stack()))
		}
	}()

	retVal = input
	for _, f := range p.Functions {
		if retVal, err = f(retVal); err != nil {
			return
		}
	}
	return
}
