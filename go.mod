module gitlab.com/blissfulreboot/golang/utilities

go 1.14

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/gopcua/opcua v0.1.11
	github.com/yuin/goldmark v1.1.30 // indirect
	gitlab.com/blissfulreboot/golang/conffee v1.0.0
	golang.org/x/crypto v0.0.0-20200429183012-4b2356b1ed79 // indirect
	golang.org/x/net v0.0.0-20200506145744-7e3656a0809f // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200509044756-6aff5f38e54f // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20200509030707-2212a7e161a5 // indirect
)
