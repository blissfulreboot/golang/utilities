package Configuration

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

var Debug = false

func printDebug(args ...interface{}) {
	if Debug {
		log.Println(args...)
	}
}

func getEnvironmentVariableName(str string) string {
	snake := regexp.MustCompile("(.)([A-Z][a-z]+)").ReplaceAllString(str, "${1}_${2}")
	snake  = regexp.MustCompile("([a-z0-9])([A-Z])").ReplaceAllString(snake, "${1}_${2}")
	return strings.ToUpper(snake)
}

func readConfiguration(configurationFileName string) ([]byte, error) {
	var file []byte
	var err error

	configurationFiles := []string{"./" + configurationFileName, "/etc/" + configurationFileName}
	for _, c := range configurationFiles {
		file, err = ioutil.ReadFile(c)
		if err == nil {
			break
		}
	}

	return file, err
}

func parseConfiguration(fileContent []byte, target interface{}, parseEnvironment bool) error {
	val := reflect.Indirect(reflect.ValueOf(target))
	valPtr := reflect.ValueOf(target)
	if val.Kind() != reflect.Struct {
		return errors.New("Configuration target must be a struct, got " + val.Kind().String())
	}
	json.Unmarshal(fileContent, &target)

	if !parseEnvironment { return nil}

	for i := 0; i < val.Type().NumField(); i++ {
		fieldName := val.Type().Field(i).Name
		v := valPtr.Elem().FieldByName(fieldName)
		fmt.Println(fieldName, val.Type().Field(i).Type.Kind())
		if envVar := os.Getenv(getEnvironmentVariableName(fieldName)); envVar != "" {
			switch val.Type().Field(i).Type.Kind() {
			case reflect.String:
				if v.IsValid() {
					v.SetString(envVar)
				}
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				if v.IsValid() {
					parsedEnvVar, err := strconv.ParseInt(envVar, 10, 64)
					if err != nil {
						printDebug("Could not parse " + envVar + " to int")
					} else {
						v.SetInt(parsedEnvVar)
					}
				}
			case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				if v.IsValid() {
					parsedEnvVar, err := strconv.ParseUint(envVar, 10, 64)
					if err != nil {
						printDebug("Could not parse " + envVar + " to uint")
					} else {
						v.SetUint(parsedEnvVar)
					}
				}
			case reflect.Float32, reflect.Float64:
				if v.IsValid() {
					parsedEnvVar, err := strconv.ParseFloat(envVar, 64)
					if err != nil {
						printDebug("Could not parse " + envVar + " to float")
					} else {
						v.SetFloat(parsedEnvVar)
					}
				}
			case reflect.Bool:
				if v.IsValid() {
					parsedEnvVar, err := strconv.ParseBool(envVar)
					if err != nil {
						printDebug("Could not parse " + envVar + " to bool")
					} else {
						v.SetBool(parsedEnvVar)
					}
				}
			default:
				printDebug("Detected unknown/unsupported type: " + val.Type().Field(i).Type.String())
				fmt.Println("Detected unknown/unsupported type: " + val.Type().Field(i).Type.String())
			}
		}
	}
	return nil
}

func ReadConfiguration(configurationFileName string, target interface{}, requireConfigurationFile bool, parseEnvironment bool) error {
	f, e := readConfiguration(configurationFileName)
	if e != nil {
		printDebug(fmt.Sprintf("File error: %v", e))
		if requireConfigurationFile {
			return e
		}
	}
	return parseConfiguration(f, target, parseEnvironment)
}
