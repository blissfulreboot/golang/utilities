package main

import (
	"fmt"
	"gitlab.com/blissfulreboot/golang/utilities/MQTT"
	"os"
	"strconv"
	"time"
)

func main() {
	client := MQTT.
		NewMqttConnection("my-test-client", "192.168.2.35", 1883).
		SetSubscriberQoS(2).
		SetPublisherQos(2).
		SetDebugging(true)

	debugChannel := client.DebugChannel
	go func() {
		for item := range debugChannel {
			fmt.Println(item)
		}
	}()

	writeThis := make(chan string, 1000000)
	go func() {
		for i := 0; ; i++ {
			writeThis <- strconv.Itoa(i)
			time.Sleep(2 * time.Second)
		}
	}()
	if err := client.Publish("my-test-topic", false, writeThis); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	readThis := make(chan []byte, 1000000)
	if err := client.Subscribe("my-test-topic", readThis); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	go func() {
		for item := range readThis {
			fmt.Println("Subscriber: " + string(item))
		}
	}()
	select {}
}
